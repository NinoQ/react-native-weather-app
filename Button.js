import React from "react";
import { StyleSheet, Text, Pressable } from "react-native";

export default function Button(props) {
  return (
    <Pressable
      title={props.city}
      onPress={() => {
        props.setCity(props.city);
      }}
      style={styles.button}
    >
      <Text style={{ fontWeight: "bold", fontSize: 16 }}>{props.city}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    margin: 30,
    padding: 8,
    backgroundColor: "#ccccff",
    color: "#000000",
    borderRadius: 10,
  },
});

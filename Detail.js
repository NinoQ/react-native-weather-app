import React from "react";
import { StyleSheet, Text, View, Pressable } from "react-native";

export default function Detail(props) {
  return (
    <View style={styles.detail}>
      <Text style={{ fontWeight: "bold", color: "#FFF" }}>{props.detail}</Text>
      <Text style={{ fontWeight: "bold", color: "#FFF" }}>{props.value}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  detail: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
  },
});

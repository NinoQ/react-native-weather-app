import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  Pressable,
  Image,
} from "react-native";
import { API_KEY } from "./ApiKey";
import axios from "axios";
import Detail from "./Detail";
import Button from "./Button";
import SevenDayForecast from "./SevenDayForecast";


export default function App() {
  const [city, setCity] = useState("Tbilisi");
  const [cityData, setCityData] = useState(null);
  const [loadMoreClicked, setLoadMoreClicked] = useState(false);

  const getCoordinates = (city) => {
    switch (city) {
      case "Tbilisi":
        return { lat: "41.7151", lon: "44.8271" };
      case "Kutaisi":
        return { lat: "42.2662", lon: "42.718" };
      case "Batumi":
        return { lat: "41.6367", lon: "41.6168" };
    }
  };

  const cities = ["Tbilisi", "Batumi", "Kutaisi"];

  useEffect(() => {
    axios
      .post(
        "https://api.openweathermap.org/data/2.5/onecall?lat=" +
          getCoordinates(city).lat +
          "&lon=" +
          getCoordinates(city).lon +
          "&units=metric&exclude=minutely&appid=" +
          API_KEY
      )
      .then((response) => {
        setCityData(response);
      });
  }, [city]);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.buttons}>
        {cities.map(function (city) {
          return <Button key={city} city={city} setCity={setCity} />;
        })}
        <StatusBar style="auto" />
      </View>
      <View style={styles.details}>
        <Text
          style={{
            fontWeight: "bold",
            color: "#FFF",
            marginBottom: 20,
            fontSize: 20,
          }}
        >
          {city}
        </Text>
        <Image
          style={styles.logo}
          source={{
            uri: cityData
              ? "https://openweathermap.org/img/wn/" +
                cityData.data.current.weather[0].icon +
                ".png"
              : null,
          }}
        />
        <Detail
          detail={"Temperature"}
          value={cityData ? cityData.data.current.temp + "°" : null}
        />
        <Detail
          detail={"Feels Like"}
          value={cityData ? cityData.data.current.feels_like + "°" : null}
        />
        <Detail
          detail={"Humidity"}
          value={cityData ? cityData.data.current.humidity + "%" : null}
        />
        <Detail
          detail={"Pressure"}
          value={cityData ? cityData.data.current.pressure : null}
        />
      </View>
      <Pressable
        title="Load 7 days forecast data"
        onPress={() => {
          setLoadMoreClicked(!loadMoreClicked);
        }}
        style={styles.loadMoreButton}
      >
        <Text style={styles.loadMoreText}>Load 7 days forecast data</Text>
      </Pressable>
      <View style={styles.sevenDayForecastContainer}>
        {loadMoreClicked ? (
          <SevenDayForecast daily={cityData.data.daily.slice(1, 8)} />
        ) : null}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#240050",
  },
  buttons: {
    display: "flex",
    justifyContent: "space-around",
    flexDirection: "row",
    marginTop: 30,
  },
  details: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: 10,
  },
  detail: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
  },
  logo: {
    width: 100,
    height: 100,
  },
  loadMoreButton: {
    display: "flex",
    alignItems: "center",
    borderRadius: 10,
  },
  loadMoreText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#ccccff",
  },
  sevenDayForecastContainer: {
    display: "flex",
    flexDirection: "column",
    marginTop: 10,
  },
});


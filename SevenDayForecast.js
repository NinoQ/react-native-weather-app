import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

export default function SevenDayForecast(props) {
  return (
    <>
      {props.daily
        ? props.daily.map((d) => {
            return (
              <View key={d.dt} style={styles.oneDayForecastContainer}>
                <Text style={styles.text}>
                  {new Date(d.dt * 1000).toDateString()}
                </Text>
                <Image
                  style={styles.logo}
                  source={{
                    uri:
                      "https://openweathermap.org/img/wn/" +
                      d.weather[0].icon +
                      ".png",
                  }}
                />
                <Text style={styles.text}>{d.temp.day + "°"}</Text>
                <Text style={styles.text}>{d.weather[0].description}</Text>
              </View>
            );
          })
        : null}
    </>
  );
}

const styles = StyleSheet.create({
  oneDayForecastContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 5,
  },
  text: {
    fontWeight: "bold",
    fontSize: 16,
    color: "#FFF",
    display: "flex",
    alignItems: "center",
  },
  logo: {
    width: 30,
    height: 30,
    display: "flex",
    alignItems: "center",
  },
});
